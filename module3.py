import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats


def get_count_values_frame(dfr, col_name):
    frame = dfr[col_name].value_counts().to_frame()
    frame.rename(columns={col_name: 'value_counts'}, inplace=True)
    frame.index.name = col_name
    return frame


pd.set_option('display.width', 1000)

df = pd.read_csv('clean_df.csv')
print df.dtypes
print df[['bore', 'stroke', 'compression-ratio', 'horsepower']].corr()

# Engine size as potential predictor variable of price
#sns.regplot(x="engine-size", y="price", data=df)

# Highway mpg as a potential predictor variable of price
#sns.regplot(x="highway-mpg", y="price", data=df)

# Peak rpm as a predictor variable of price
#sns.regplot(x="peak-rpm", y="price", data=df)

# Peak stroke as a predictor variable of price
print df[['stroke', 'price']].corr()
#sns.regplot(x="stroke", y="price", data=df)
#plt.show()

# body-style
#sns.boxplot(x="body-style", y="price", data=df)
# engine-location
#sns.boxplot(x="engine-location", y="price", data=df)
# drive-wheels
#sns.boxplot(x="drive-wheels", y="price", data=df)
#plt.show()

# ** Descriptive Statistical Analysis **
print '\nDescriptive Statistical Analysis\n'

# describe object values
print '\nDescribe all object-type characteristics'
print df.describe(include=['object'])

# drive-wheels as variable
print '\nValue count for drive-wheels'
print get_count_values_frame(df, 'drive-wheels')
print get_count_values_frame(df, 'engine-location')
print df['engine-location'].value_counts()


# ** Basic of Grouping **
print '\nBasic of Grouping\n'
print df['drive-wheels'].unique()

# grouping results
df_group_one = df[['drive-wheels', 'body-style', 'price']]
df_group_one = df_group_one.groupby(['drive-wheels', 'body-style'], as_index=False).mean()
print df_group_one

# grouping results
df_gptest = df[['drive-wheels', 'body-style', 'price']]
grouped_test1 = df_gptest.groupby(['drive-wheels', 'body-style'], as_index=False).mean()
print grouped_test1

grouped_pivot = df_group_one.pivot(index='drive-wheels', columns='body-style')
grouped_pivot = grouped_pivot.fillna(0)
print grouped_pivot

print 'the average "price" of each car based on "body-style"'
df_group = df_group_one.groupby('body-style').mean()
print df_group


# ** Correlation and Causation **
print '\n  Correlation and Causation\n'

# use the grouped results
#plt.pcolor(grouped_pivot, cmap='RdBu')
#plt.colorbar()
#plt.show()

fig, ax = plt.subplots()
im = ax.pcolor(grouped_pivot, cmap='RdBu')
# label names
row_labels = grouped_pivot.columns.levels[1]
col_labels = grouped_pivot.index
# move ticks and labels to the center
ax.set_xticks(np.arange(grouped_pivot.shape[1])+0.5, minor=False)
ax.set_yticks(np.arange(grouped_pivot.shape[0])+0.5, minor=False)
# insert labels
ax.set_xticklabels(row_labels, minor=False)
ax.set_yticklabels(col_labels, minor=False)
# rotate label if too long
plt.xticks(rotation=90)

fig.colorbar(im)
#plt.show()


pearson_coef, p_value = stats.pearsonr(df['wheel-base'], df['price'])
print("The Pearson Correlation Coefficient is", pearson_coef, " with a P-value of P =", p_value)

pearson_coef, p_value = stats.pearsonr(df['horsepower'], df['price'])
print("The Pearson Correlation Coefficient is", str(pearson_coef), " with a P-value of P =", str(p_value))

# my experiment
df_count = df.count()[0]

matrix = []
df1 = df._get_numeric_data()
df2 = df._get_numeric_data()
for i in df1:
    new = []
    for j in df2:
        a1 = df[i]
        b1 = df[j]
        r, p = stats.pearsonr(a1, b1)

        new.append(str(r) + ' ; ' + str(p))
    matrix.append(new)

#print matrix[0]


# ** ANOVA **
print '** ANOVA **'
grouped_test2 = df_gptest[['drive-wheels', 'price']].groupby(['drive-wheels'])
print grouped_test2.head(2)
grouped_test2.get_group('4wd')['price']

# ANOVA
f_val, p_val = stats.f_oneway(grouped_test2.get_group('fwd')['price'], grouped_test2.get_group('rwd')['price'],
                              grouped_test2.get_group('4wd')['price'])
print("ANOVA results: F=", f_val, ", P =", p_val)
f_val, p_val = stats.f_oneway(grouped_test2.get_group('fwd')['price'], grouped_test2.get_group('rwd')['price'])
print("ANOVA results: F=", f_val, ", P =", p_val)
f_val, p_val = stats.f_oneway(grouped_test2.get_group('4wd')['price'], grouped_test2.get_group('rwd')['price'])
print("ANOVA results: F=", f_val, ", P =", p_val)
f_val, p_val = stats.f_oneway(grouped_test2.get_group('4wd')['price'], grouped_test2.get_group('fwd')['price'])
print("ANOVA results: F=", f_val, ", P =", p_val)

