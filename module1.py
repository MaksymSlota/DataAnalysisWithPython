import pandas as pd

def downloadCarsData():

    path="https://archive.ics.uci.edu/ml/machine-learning-databases/autos/imports-85.data"
    df = pd.read_csv(path,header=None)
    # create headers list
    headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]
    # replace headers
    df.columns = headers
    return df


def get_header():
    headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]
    return headers
#pandas options
#pd.set_option('display.height', 1000)
#pd.set_option('display.max_rows', 500)
#pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)




df1 = downloadCarsData()
#print df1.dtypes
#print df1.info()

#print 'head'
#print df.head(2)

#print 'describe'
#print df.describe(include="all")

#print df[[0, 1, 5]].describe(include="all")