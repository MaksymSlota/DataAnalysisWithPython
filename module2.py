import numpy as np
import pandas as pd
import matplotlib as plt
from matplotlib import pyplot
import module1


def print_missing_values(dfr):
    missing_data = dfr.isnull()
    for column in missing_data.columns.values.tolist():
        print(column)
        print(missing_data[column].value_counts())
        print("")
    return


def replace_by_mean(dfr):
    for column in dfr:
        try:
            avg = dfr[column].astype("float").mean(axis=0)
            dfr[column].replace(np.nan, avg, inplace=True)
        except:
            print 'error' + str(column)
    return


def replace_with_dummy_var(dfr, coll_name, dummy_val1, dummy_val2):
    dummy_variable = pd.get_dummies(dfr[coll_name])
    # change column names for clarity
    dummy_variable.rename(columns={'column': dummy_val1, 'column': dummy_val2}, inplace=True)
    # merge data frame "df" and "dummy_variable_1"
    dfr = pd.concat([dfr, dummy_variable], axis=1)
    # drop original column "fuel-type" from "df"
    dfr.drop(coll_name, axis=1, inplace=True)
    return dfr


# ** download data **
df = module1.downloadCarsData()
print df

# ** replace ? values **
df.replace("?", np.nan, inplace=True)
print_missing_values(df)

# ** Correct data format **
replace_by_mean(df)
# check missing values after replacing
print_missing_values(df)

# ** correct types **
df[["bore", "stroke", "horsepower"]] = df[["bore", "stroke", "horsepower"]].astype("float")
df[["normalized-losses"]] = df[["normalized-losses"]].astype("int")
df[["price"]] = df[["price"]].astype("float")
df[["peak-rpm"]] = df[["peak-rpm"]].astype("float")
print df.dtypes

# ** data standardization **
df['city-L/100km'] = 235/df["city-mpg"]
df['highway-L/100km'] = 235/df["highway-mpg"]

# ** Data Normalization **
print 'Data Normalization'
df['length'] = df['length']/df['length'].max()
df['width'] = df['width']/df['width'].max()
df['height'] = df['height']/df['height'].max()
print df[['length', 'width', 'height']].head(5)

# ** Binning **
binwidth = (max(df["horsepower"])-min(df["horsepower"]))/4
bins = np.arange(min(df["horsepower"]), max(df["horsepower"]), binwidth)
group_names = ['Low', 'Medium', 'High']
df['horsepower-binned'] = pd.cut(df['horsepower'], bins, labels=group_names, include_lowest=True)

# ** Bins Visualization **
a = (0, 1, 2)
# draw historgram of attribute "horsepower" with bins = 3
plt.pyplot.hist(df["horsepower"], bins=3)
# set x/y labels and plot title
plt.pyplot.xlabel("horsepower")
plt.pyplot.ylabel("count")
plt.pyplot.title("horsepower bins")
#plt.pyplot.show()

# ** Indicator Variables **
dummy_variable_1 = pd.get_dummies(df["fuel-type"])
# change column names for clarity
dummy_variable_1.rename(columns={'fuel-type-diesel':'gas', 'fuel-type-diesel':'diesel'}, inplace=True)
# show first 5 instances of data frame "dummy_variable_1"
dummy_variable_1.head()
# merge data frame "df" and "dummy_variable_1"
df = pd.concat([df, dummy_variable_1], axis=1)
# drop original column "fuel-type" from "df"
df.drop("fuel-type", axis = 1, inplace=True)


d = replace_with_dummy_var(df, 'aspiration', 'std', 'turbo')

print d.head()

d.to_csv('clean_df.csv')